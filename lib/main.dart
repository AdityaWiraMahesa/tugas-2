import 'dart:ui';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal,
        body: Column(
          children: <Widget>[
            Container(
              height: 30,
              margin: EdgeInsets.only(top: 40),
              child: Center(
                child: new Text(
                  "Profile",
                  style: TextStyle(
                      fontSize: 30, fontFamily: 'Script', color: Colors.white),
                ),
              ),
            ),

            //Container 1
            Container(
              height: 140,
              color: Colors.teal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                      borderRadius: new BorderRadius.circular(100),
                      child: Image.asset(
                        "assets/Profile.jpg",
                        width: 120.0,
                        height: 120.0,
                        fit: BoxFit.cover,
                      )),
                ],
              ),
            ),

            //Container 2
            Container(
              height: 70,
              decoration: BoxDecoration(),
              child: Column(
                children: <Widget>[
                  Text(
                    "I Made Aditya Wira Mahesa",
                    style: TextStyle(
                        fontSize: 25,
                        fontFamily: 'Myriad',
                        color: Colors.white),
                  ),
                  Text(
                    "'Small step of goodwill can take us beyond our imagination'",
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 15,
                        color: Colors.white),
                  ),
                  Container(
                    width: 300,
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(width: 0.9, color: Colors.white))),
                  )
                ],
              ),
            ),

            //Container 3
            Container(
              padding: const EdgeInsets.only(top: 10),
              height: 450,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: Column(
                children: <Widget>[
                  Text(
                    "About Me",
                    style: TextStyle(fontStyle: FontStyle.italic, fontSize: 17),
                  ),

                  //KARANGASEM
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: const EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset:
                                    Offset(0, 2), // changes position of shadow
                              )
                            ]),
                        width: 150,
                        height: 150,
                        child: Column(
                          children: <Widget>[
                            Icon(
                              Icons.my_location,
                              size: 70,
                              color: Colors.red,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.only(top: 10),
                              alignment: Alignment.center,
                              width: 130,
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  border: Border(
                                      top: BorderSide(
                                          width: 0.9, color: Colors.black))),
                              child: Text(
                                "Karangasem",
                                style: TextStyle(
                                    fontFamily: 'Myriad', fontSize: 25),
                              ),
                            )
                          ],
                        ),
                      ),

                      //INFORMATIKA
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: const EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset:
                                    Offset(0, 2), // changes position of shadow
                              )
                            ]),
                        width: 150,
                        height: 150,
                        child: Column(
                          children: <Widget>[
                            Icon(
                              Icons.work,
                              size: 70,
                              color: Colors.blue,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.only(top: 10),
                              alignment: Alignment.center,
                              width: 130,
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  border: Border(
                                      top: BorderSide(
                                          width: 0.9, color: Colors.black))),
                              child: Text(
                                "Informatika",
                                style: TextStyle(
                                    fontFamily: 'Myriad', fontSize: 25),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )),

                  //UNDIKSHA
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: const EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset:
                                    Offset(0, 2), // changes position of shadow
                              )
                            ]),
                        width: 150,
                        height: 150,
                        child: Column(
                          children: <Widget>[
                            Icon(
                              Icons.location_city,
                              size: 70,
                              color: Colors.black,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.only(top: 10),
                              alignment: Alignment.center,
                              width: 130,
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  border: Border(
                                      top: BorderSide(
                                          width: 0.9, color: Colors.black))),
                              child: Text(
                                "Undiksha",
                                style: TextStyle(
                                    fontFamily: 'Myriad', fontSize: 25),
                              ),
                            )
                          ],
                        ),
                      ),

                      //SINGLE
                      Container(
                        margin: EdgeInsets.all(20),
                        padding: const EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                spreadRadius: 1,
                                blurRadius: 10,
                                offset:
                                    Offset(0, 2), // changes position of shadow
                              )
                            ]),
                        width: 150,
                        height: 150,
                        child: Column(
                          children: <Widget>[
                            Icon(
                              Icons.people,
                              size: 70,
                              color: Colors.pinkAccent,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: const EdgeInsets.only(top: 10),
                              alignment: Alignment.center,
                              width: 130,
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  border: Border(
                                      top: BorderSide(
                                          width: 0.9, color: Colors.black))),
                              child: Text(
                                "Single",
                                style: TextStyle(
                                    fontFamily: 'Myriad', fontSize: 25),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )),

                  //Media Social
                  Container(
                    height: 40,
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //IG
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/IG.png",
                                width: 20,
                                height: 20,
                              ),
                              Text(" adithyawm_",
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold))
                            ],
                          ),
                        ),

                        //FB
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/FB.png",
                                width: 20,
                                height: 20,
                              ),
                              Text(
                                " Adithya",
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
